# Introduction 
Cypress is a framework for end-to-end test automation, where it currently uses the JavaScript language and runs on various browsers such as Chrome, Firefox, and Edge.

# Facilities required to start the Automation project in Cypress
Download Node.js (click https://nodejs.org/pt-br/download/)
After downloading, just perform next/next until the end.
Download npm (JavaScript package manager).
Download Cypress (run: npm install cypress)
Text editor or IDE (we'll use VS Code, click https://code.visualstudio.com/download).

# Check to see if everything is installed
Open the cmd and enter the commands below:
node -version
npm -version

# Run cypress 
Cypress will open a fairly user-friendly interface to track test execution while specs files are edited.
Execute the command via cmd: npx run open  
Click on the test and watch the scenario run.